(function () {
  let _sourceBufferList = []

  // 监听资源全部录取成功
  let _endOfStream = window.MediaSource.prototype.endOfStream
  window.MediaSource.prototype.endOfStream = function () {
    // alert('资源全部捕获成功，即将下载！')
    // _download()
     _endOfStream.call(this)
  }

  // 捕获资源
  let _addSourceBuffer = window.MediaSource.prototype.addSourceBuffer
  window.MediaSource.prototype.addSourceBuffer = function (mime) {
    console.log("addSourceBuffer", mime)
    let sourceBuffer = _addSourceBuffer.call(this, mime)
    let _append = sourceBuffer.appendBuffer
    let bufferList = []
    let sliceDownloadCount = 1
    const index = _sourceBufferList.length

    _sourceBufferList.push({
      mime,
      bufferList,
      sliceDownloadCount
    })

    sourceBuffer.appendBuffer = function (buffer) {
      bufferList.push(buffer)
      setSourceInfo()
      _append.call(this, buffer)
    }
    return sourceBuffer
  }

  const setSourceInfo = () => {
      let text = _sourceBufferList.map(source => {
        return `[${source.mime.split(';')[0]}] ${source.bufferList.length}`
      }).join('<br/>')
      $downloadNum.innerHTML = text
  }

  let $downloadNum = document.createElement('div')


  // 添加操作的 dom
  function _appendDom () {
    const textStyle = `
      position: fixed;
      top: 50px;
      left: 50px;
      z-index: 9999;
      color: white;
      font-size: 12px;
      font-weight: bold;
      text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
    `

    $downloadNum.innerHTML = '開始進行擷取'
    $downloadNum.style = textStyle
    document.getElementsByTagName('html')[0].insertBefore($downloadNum, document.getElementsByTagName('head')[0]);

  }

  _appendDom()
})()