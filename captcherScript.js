(function () {
  const fileName = "blobShard"
  const sliceCount = 300
  const sliceDownloadOnCapture = true
  const sliceDownload = false
  const downloadOnStreamEnd = true

  let _sourceBufferList = []
  let isTenRatePlay = false
  function _tenRatePlay () {
    let newPlayrate = isTenRatePlay? 1: 10
    $tenRate.innerHTML = isTenRatePlay?'十倍速前進' : '回復一倍速'
    isTenRatePlay = !isTenRatePlay
    let $domList = document.getElementsByTagName('video')
    for (let i = 0, length = $domList.length; i < length; i++) {
      const $dom = $domList[i]
      $dom.playbackRate = newPlayrate
    }
  }

  function _provideBlob ({fileName, shardCount, buffer}) {
    const fileBlob = new Blob(buffer, { type: "application/octet-stream" }) // 統一存成binary file
    const a = document.createElement('a')
    a.download = shardCount > 0 ? `${fileName}.${shardCount}` : `${fileName}`
    a.href = URL.createObjectURL(fileBlob)
    a.style.display = 'none'
    document.body.appendChild(a)
    a.click()
    a.remove()
  }

  // 下载捕获到的资源
  function _download () {
    _sourceBufferList.slice(-2).forEach((target) => { // 只處裡最新兩個media source
      const mime = target.mime.split(';')[0] //  audio/mp4 or  video/mp4
      const type = mime.split('/')[1]
      const blobFileName = `${fileName || document.title}.${mime.split('/')[0]}.${mime.split('/')[1]}`

      if (sliceDownloadOnCapture) {
        if (target.sliceDownloadCount > 1) { // 已經有在抓取途中下載的情況, 下載最後一個分片
          _provideBlob({
            fileName: blobFileName,
            shardCount: target.sliceDownloadCount,
            buffer: target.bufferList,
          })
        }
        else {
          _provideBlob({
            fileName: blobFileName,
            shardCount: 0,
            buffer: target.bufferList,
          })
        }
      } else if (sliceDownload) {
        if ( target.bufferList.length <= sliceCount) {
          _provideBlob({
            fileName: blobFileName,
            shardCount: 0,
            buffer:  target.bufferList,
          })
        } else {
          let copyBuffer = target.bufferList.slice()
          let shardCount = 1
          while (copyBuffer.length > 0) {
            _provideBlob({
              fileName: blobFileName,
              shardCount: shardCount,
              buffer: copyBuffer.slice(0, sliceCount),
            })
            copyBuffer = copyBuffer.slice(sliceCount)
            shardCount += 1
          }
        }
      } else {
          console.log("normal download")
          _provideBlob({
            fileName: blobFileName,
            shardCount: 0,
            buffer: target.bufferList,
          })
      }
    })
  }

  // overide endOfStream
  let _endOfStream = window.MediaSource.prototype.endOfStream
  window.MediaSource.prototype.endOfStream = function () {
    if (sliceDownloadOnCapture || downloadOnStreamEnd) {
      _download()
    }
    else {
      alert('擷取完成')
    }
    _endOfStream.call(this)
  }

  // overide addSourceBuffer & appendBuffer
  let _addSourceBuffer = window.MediaSource.prototype.addSourceBuffer
  window.MediaSource.prototype.addSourceBuffer = function (mime) {
    console.log("addSourceBuffer", mime)
    let sourceBuffer = _addSourceBuffer.call(this, mime)
    let _append = sourceBuffer.appendBuffer
    let bufferList = []
    let sliceDownloadCount = 1
    const index = _sourceBufferList.length

    _sourceBufferList.push({
      mime,
      bufferList,
      sliceDownloadCount
    })

    sourceBuffer.appendBuffer = function (buffer) {
      if (sliceDownloadOnCapture && bufferList.length >= sliceCount) {
          const fileMime = mime.split(';')[0]
          const blobFileName = `${fileName || document.title}.${fileMime.split('/')[0]}.${fileMime.split('/')[1]}`
          console.log(`prepare download ${blobFileName}.${sliceDownloadCount}`)
          _provideBlob({
            fileName: blobFileName,
            shardCount: sliceDownloadCount,
            buffer: bufferList.slice(),
            mime: fileMime
          })
          sliceDownloadCount += 1
          _sourceBufferList[index].sliceDownloadCount = sliceDownloadCount

          console.log("clean old buffer")
          while (bufferList.length) {
            bufferList.pop();
          }
      }

      bufferList.push(buffer)
      setSourceInfo()
      _append.call(this, buffer)
    }
    return sourceBuffer
  }

  const setSourceInfo = () => {
      let text = _sourceBufferList.map(source => {
        return `[${source.mime.split(';')[0]}] ${source.bufferList.length}`
      }).join('<br/>')
      $downloadNum.innerHTML = text
  }

  let $btnDownload = document.createElement('div')
  let $downloadNum = document.createElement('div')
  let $tenRate = document.createElement('div')
  let $clearSource = document.createElement('div')


  function logSourceBufferList () {
    console.log(_sourceBufferList)
  }
  let $test = document.createElement('div')


  // 添加操作的 dom
  function _appendDom () {
    const baseStyle = `
      position: fixed;
      top: 50px;
      right: 50px;
      height: 40px;
      padding: 0 20px;
      z-index: 9999;
      color: white;
      cursor: pointer;
      font-size: 16px;
      font-weight: bold;
      line-height: 40px;
      text-align: center;
      border-radius: 4px;
      background-color: #3498db;
      box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.3);
    `
    const textStyle = `
      position: fixed;
      top: 50px;
      left: 50px;
      z-index: 9999;
      color: white;
      font-size: 12px;
      font-weight: bold;
      text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
    `

    $downloadNum.innerHTML = '開始進行擷取'
    $downloadNum.style = textStyle

    $tenRate.innerHTML = '十倍速前進'
    $tenRate.style = baseStyle + `top: 100px;`
    $tenRate.addEventListener('click', _tenRatePlay)

    $btnDownload.innerHTML = '存取已擷取片段'
    $btnDownload.style = baseStyle + `top: 150px;`
    $btnDownload.addEventListener('click', _download)

    $test.innerHTML = 'print buffer'
    $test.style = baseStyle + `top: 250px;`
    $test.addEventListener('click', logSourceBufferList)
    document.getElementsByTagName('html')[0].insertBefore($test, document.getElementsByTagName('head')[0]);

    document.getElementsByTagName('html')[0].insertBefore($downloadNum, document.getElementsByTagName('head')[0]);
    document.getElementsByTagName('html')[0].insertBefore($tenRate, document.getElementsByTagName('head')[0]);
    document.getElementsByTagName('html')[0].insertBefore($btnDownload, document.getElementsByTagName('head')[0]);

  }

  _appendDom()

  /**
  一些網站的實際使用說明

  * 油管
  因為油管切換畫質時會使用新的MediaSource，所以使用時只要打開console貼上程式碼，切換畫質後就可以開始進行擷取。
  不過因為會自動往下播放 所以建議開啟串流結束後自動下載的選項

  * 如果你是為了看某個串流很卡又鎖IP的本丸的話
  工程師很善良很好手工中斷。
  打開串流播放，預設畫質是auto，大小會跳來跳去，建議在打開後切換畫質。
  切換畫質後馬上關掉VPN會導致後面撥放器抓取擷圖失敗，建議從console確認抓取完畢後再中斷連結。
  全螢幕的時候附加的控制按鈕會消失，但還是會繼續擷取，可以安心觀看

  */
})()