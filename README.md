## 說明
* 基於[无差别视频提取工具 by momo707577045](https://segmentfault.com/a/1190000025182822)修改而成
* 為了某些切換畫質會中斷的串流網站和電腦配備停留在上一個世代，記憶體賣家又還沒發貨C槽滿出來配信要到期的使用者增加了`分段下載`(擷取時分段下載或者最後下載時分段)的功能。
* 因為某些網站切換選項之後會使用新的MediaSource，點擊下載時僅處理最新的兩個MediaSource。
* 因為等很久還是會想看串流，把十倍速按鈕改成了可以切換。
* 支援以[MediaSource](https://developer.mozilla.org/zh-TW/docs/Web/API/MediaSource) 為基礎的各種串流網站


## 使用
### 環境
* 擷取檔案時需要可以使用瀏覽器下中斷點執行js
* 合併檔案：使用python
* 播放檔案/轉換檔案時：使用VLC

### 確認參數
captcherScript.js
```js
  const fileName = "blobShard"				// 預設的檔案名稱，沒有給或者是空值的話會抓瀏覽器的title
  const sliceCount = 5					// 幾個串流的片段會被切成一個分檔
  const sliceDownloadOnCapture = false			// 擷取中自動分片下載
  const sliceDownload = true				// 分片下載
  const downloadOnStreamEnd = true			// 串流結束時自動下載
```
sliceDownloadOnCapture的優先順序大於sliceDownload

### 擷取串流影音
過程參考[无差别视频提取工具 by momo707577045](https://segmentfault.com/a/1190000025182822)
只是貼上的程式碼是captcherScript.js的內容

如果開啟了擷取中分段下載的選項(sliceDownloadOnCaptureMode = true)，串流結束時會下載最後一個分檔
* 應該會得到blobShard.audio.mp4.1, blobShard.audio.mp4.2這樣的下載檔案
* 建議擷取的時候不要亂拉進度條
* 中斷的話要重來....

### 合併檔案
將所有分檔複製到files資料夾下，並執行

* `python merge.py --dryrun` 先確認讀取的分檔是否正確

* `python merge.py` 合併檔案，執行完畢後應該會得到兩個檔案blobShard.audio.mp4 和 blobShard.video.mp4，分別是影像檔案和聲音檔案

### 撥放&轉換
* 推薦使用[VLC](https://www.videolan.org/index.zh-TW.html)

```text
媒體	> 開啟多個檔案 > 加入檔案 > 選擇 .video.mp4
	> 勾選『顯示更多選項』 > 勾選『同步播放另一個媒體(額外的音訊檔案)』 > 選擇.audio.mp4
	> 按下播放直接觀看，或者選擇轉換讓程式產出合併的檔案
```

* ffmpeg
	* ref
		* https://superuser.com/questions/277642/how-to-merge-audio-and-video-file-in-ffmpeg

 	* 取得影像資訊
 	`ffprobe -show_data -hide_banner blobShard.video.mp4`

 	* 合併檔案
 	`ffmpeg -i video.mp4 -i audio.mp4 -c copy output.mp4`

 	* 指定第一個檔案為影像第二個檔案為聲音 直接複製內容不重新編碼
 	`ffmpeg -i blobShard.video.mp4 -i blobShard.audio.mp4 -map 0:v -map 1:a -c:v copy -c:a copy output.mp4`

	* install on Window
		> windows為什麼沒有homebrewㄋ
		* 下載好心人build好的檔案 
			https://github.com/BtbN/FFmpeg-Builds/releases
		* 找個地方解壓縮, 然後把指令換成
		` path\to\folder\bin\ffmpeg.exe -i blobShard.video.mp4 -i blobShard.audio.mp4 -map 0:v -map 1:a -c:v copy -c:a copy output.mp4`
		當然你也可以把這個位置設進windows的環境參數path裡

## 註
本項目僅用於學習，交流，切勿用於侵權行為。
