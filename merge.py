
import glob, os, sys

targetFolder = "./files"
# targetFolder = "./touken"

def init():
	os.chdir(targetFolder)

def findFiles(fileType):
	result = []

	for file in glob.glob(f"*{fileType}.*.[0-9]*"):
		result.append(file)
	# sort by last number
	result = sorted(result, key=lambda str: int(str.split('.')[-1]))
	return result

def  mergeFile(filenames):
	outfile = '.'.join(filenames[0].split('.')[:-1])
	outputPath = f"./{outfile}"

	print("start mergeFile:", filenames)
	print("merge to", outputPath)

	with open(outputPath, 'wb') as outfile:
		for fname in filenames:
			with open(fname, "rb") as infile:

				outfile.write(infile.read())
				print(f"read {fname} done")

	print("merge to", outputPath, "done")


if __name__ == '__main__':
	isDryRun = len(sys.argv) > 1 and sys.argv[1] == "--dryrun"

	init()

	fileList = findFiles(fileType="video")
	if isDryRun:
		print(fileList)
	else:
		mergeFile(filenames=fileList)

	fileList = findFiles(fileType="audio")
	if isDryRun:
		print(fileList)
	else:
		mergeFile(filenames=fileList)
